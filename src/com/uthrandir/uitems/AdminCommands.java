/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author lars
 */
public class AdminCommands implements CommandExecutor {

   private UItems plugin;

   public AdminCommands(UItems plugin) {
      this.plugin = plugin;
      plugin.getCommand("uitemsadmin").setExecutor(this);
   }

   @Override
   public boolean onCommand(CommandSender cs, Command cmnd, String label, String[] args) {
      Player p = null;
      if (cs instanceof Player) {
         p = (Player) cs;
      }

      if (args[0].equalsIgnoreCase("getitem")) {
         if (args.length != 3) {
            Logger.msg(cs, "Usage: /uitemsadmin getitem <name> <amount>");
            Logger.msg(cs, "Current uItems: gold");
            return true;
         }
         int amount;
         try {
            amount = Integer.parseInt(args[2]);
         } catch (NumberFormatException e) {
            Logger.msgError(cs, args[2] + " is not a number!");
            return true;
         }         
         
         return true;
      }

      return true;
   }

}
