/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import com.uthrandir.ucore.Utilities;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author lars
 */
public class PlayerListener implements Listener {

   @EventHandler(priority = EventPriority.MONITOR)
   void onPlace(BlockPlaceEvent ev) {
      if (ev.isCancelled()) {
         return;
      }
      if (ev.getItemInHand() == null) {
         return;
      }
      UItem uItem = UItems.inst.uitemmanager.getUItem(ev.getItemInHand());
      if (uItem == null) {
         return;
      }
      if (uItem.getType() != null && uItem.getType().equals(UItemType.Mystery_Box)) {
         onMysteryBox(ev, uItem);
      }
   }

   void onMysteryBox(BlockPlaceEvent ev, UItem uItem) {
      Logger.msg(ev.getPlayer(), "You have placed a Mystery Box!");
      MysteryBox box = (MysteryBox) uItem;      
      Chest chest = (Chest) ev.getBlockPlaced().getState();
      Inventory inv = chest.getBlockInventory();
      int nr = Utilities.getRandom(1, 9);
      if (nr > 9) {
         for (int i = 0; i < 3; i++) {
            inv.addItem(box.getRandomLoot());
         }
      } else if (nr > 5) {
         for (int i = 0; i < 2; i++) {
            inv.addItem(box.getRandomLoot());
         }
      } else {
         inv.addItem(box.getRandomLoot());
      }
   }

}
