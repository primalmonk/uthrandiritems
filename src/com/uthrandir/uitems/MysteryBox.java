/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import com.uthrandir.ucore.Utilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author lars
 */
public class MysteryBox extends UItem {

   private final Map<Integer, ItemStack> loot = new LinkedHashMap();
   private int modifierBuffer = 0;

   public MysteryBox(Collection<ItemStack> grayItems) {
      super("Mystery Box", Material.CHEST);
      setType(UItemType.Mystery_Box);
      setDisplayName(ChatColor.GOLD + "Mystery Box");
      getLore().add(ChatColor.BLUE + "Open this chest for a random reward!");
      for (ItemStack it : grayItems) {
         registerLoot(it, 100);
      }
      registerLoot(new ItemStack(Material.GOLD_RECORD), 10);
      registerLoot(new ItemStack(Material.GREEN_RECORD), 10);
      registerLoot(new ItemStack(Material.RECORD_3), 10);
      registerLoot(new ItemStack(Material.RECORD_4), 10);
      registerLoot(new ItemStack(Material.RECORD_5), 10);
      registerLoot(new ItemStack(Material.RECORD_6), 10);
      registerLoot(new ItemStack(Material.RECORD_7), 10);
      registerLoot(new ItemStack(Material.RECORD_8), 10);
      registerLoot(new ItemStack(Material.RECORD_9), 10);
      registerLoot(new ItemStack(Material.RECORD_10), 10);
      registerLoot(new ItemStack(Material.RECORD_11), 10);
      registerLoot(new ItemStack(Material.RECORD_12), 10);
      registerLoot(new ItemStack(Material.GOLD_INGOT, 32), 20);
      registerLoot(new ItemStack(Material.DIAMOND), 10);
      registerLoot(new ItemStack(Material.EMERALD), 10);

   }

   public void registerLoot(ItemStack it, int modifier) {
      loot.put(new Integer(modifierBuffer), it);
      modifierBuffer += modifier;
   }

   public ItemStack getRandomLoot() {
      int nr = Utilities.getRandom(1, modifierBuffer);
      ItemStack prev = null;
      for (Integer key : loot.keySet()) {
         if (key.intValue() < nr) {
            prev = loot.get(key);
         } else {
            break;
         }
      }
      return prev;
   }
}
