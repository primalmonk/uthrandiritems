/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author lars
 */
public class UItem {

   private final Material mat;
   private String displayName;
   private final String name;
   private List<String> lore = new ArrayList();
   private UItemType type;

   public UItem(String name, Material mat) {
      this.name = name;
      this.mat = mat;
   }

   public String getDisplayName() {
      return displayName;
   }

   public void setDisplayName(String displayName) {
      this.displayName = displayName;
   }

   public String getName() {
      return name;
   }

   public Material getMaterial() {
      return mat;
   }

   public List<String> getLore() {
      return lore;
   }

   public void setType(UItemType type) {
      this.type = type;
   }

   public UItemType getType() {
      return type;
   }

   public List<ItemStack> getItemStacks(int amount) {
      List<ItemStack> items = new ArrayList();
      for (int i = 0; i < amount / 64; i++) {
         items.add(getItemStack(64));
      }
      int remainder = amount % 64;
      if (remainder > 0) {
         items.add(getItemStack(remainder));
      }
      return items;
   }

   public ItemStack getItemStack(int amount) {
      if (amount > 64) {
         return null;
      }
      ItemStack item = new ItemStack(mat, amount);
      ItemMeta meta = item.getItemMeta();
      meta.setDisplayName(displayName);
      if (lore != null) {
         meta.setLore(lore);
      }
      item.setItemMeta(meta);
      return item;
   }

   public boolean isUItem(ItemStack item) {
      if (item == null) {
         return false;
      }
      if (!item.getType().equals(mat)) {
         return false;
      }
      if (!item.hasItemMeta()) {
         return false;
      }
      return item.getItemMeta().getDisplayName().equals(displayName);
   }

}
