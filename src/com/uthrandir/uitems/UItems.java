/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import com.uthrandir.ucore.UCore;
import com.uthrandir.uinstance.UInstance;
import com.uthrandir.umenus.UMenus;
import com.uthrandir.upres.UPres;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author lars
 */
public class UItems extends JavaPlugin {

   //Core Classes
   public AdminCommands admincommands;
   public UItemManager uitemmanager;
   public PlayerListener playerlistener;
   //Currency Module
   public UInstance uinst;
   public UMenus umenus;
   public UCore ucore;
   public UPres upres;
   //Stuff
   public static UItems inst;

   @Override
   public void onEnable() {
      inst = this;

      loadPlugins();

      loadClasses();
   }

   void loadClasses() {
      uitemmanager = new UItemManager();
      admincommands = new AdminCommands(this);
      playerlistener = new PlayerListener();
      getServer().getPluginManager().registerEvents(playerlistener, this);
   }

   void loadPlugins() {
      uinst = (UInstance) getServer().getPluginManager().getPlugin("UthrandirInstance");
      umenus = (UMenus) getServer().getPluginManager().getPlugin("UthrandirMenus");
      ucore = (UCore) getServer().getPluginManager().getPlugin("UthrandirCore");
      upres = (UPres) getServer().getPluginManager().getPlugin("UthrandirPrestige");
   }

}
