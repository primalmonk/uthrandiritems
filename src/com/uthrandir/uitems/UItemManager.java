/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import com.uthrandir.ucore.Utilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author lars
 */
public final class UItemManager {

   private final Map<UItemType, UItem> uItems;
   private final Map<CustomItemType, ItemStack> grayItems;

   public UItemManager() {
      uItems = new HashMap();
      grayItems = new HashMap();

      loadGrayItems();
      loadUItems();
   }

   public void registerUItem(UItem uItem) {
      uItems.put(uItem.getType(), uItem);
   }

   public UItem getUItem(UItemType type) {
      return uItems.get(type);
   }

   public UItem getUItem(ItemStack it) {
      for (UItem uItem : uItems.values()) {
         if (uItem.isUItem(it)) {
            return uItem;
         }
      }
      return null;
   }

   public Collection<ItemStack> getGrayItems() {
      return grayItems.values();
   }

   public ItemStack getGrayItem(CustomItemType type) {
      return grayItems.get(type);
   }

   void loadUItems() {
      MysteryBox box = new MysteryBox(getGrayItems());
      uItems.put(UItemType.Mystery_Box, box);
   }

   void loadGrayItems() {
      ItemStack it;
      ItemMeta meta;
      List<String> lore;

      //Old Boots
      it = new ItemStack(Material.LEATHER_BOOTS);
      meta = it.getItemMeta();
      meta.setDisplayName(ChatColor.DARK_GRAY + "Old Boots");
      lore = Utilities.stringFixer("These boots have seen better days.", 35, ChatColor.GRAY);
      meta.setLore(lore);
      it.setItemMeta(meta);
      grayItems.put(CustomItemType.Old_Boots, it);

      //Worn Pants
      it = new ItemStack(Material.LEATHER_LEGGINGS);
      meta = it.getItemMeta();
      meta.setDisplayName(ChatColor.DARK_GRAY + "Worn Pants");
      lore = Utilities.stringFixer("These pants have seen many wearers.", 35, ChatColor.GRAY);
      meta.setLore(lore);
      it.setItemMeta(meta);
      grayItems.put(CustomItemType.Worn_Pants, it);

      //Used Loicloth
      it = new ItemStack(Material.LEATHER);
      meta = it.getItemMeta();
      meta.setDisplayName(ChatColor.DARK_GRAY + "Used Loincloth");
      lore = Utilities.stringFixer("A loincloth that has been used by cavemen throughout the ages.", 35, ChatColor.GRAY);
      meta.setLore(lore);
      it.setItemMeta(meta);
      grayItems.put(CustomItemType.Used_Loincloth, it);

      //Rusy Bucket
      it = new ItemStack(Material.BUCKET);
      meta = it.getItemMeta();
      meta.setDisplayName(ChatColor.DARK_GRAY + "Rusty Bucket");
      lore = Utilities.stringFixer("A bucket that has been used to carry around water more than a couple of items.", 35, ChatColor.GRAY);
      meta.setLore(lore);
      it.setItemMeta(meta);
      grayItems.put(CustomItemType.Rusty_Bucket, it);

      //Blanket
      it = new ItemStack(Material.CARPET);
      meta = it.getItemMeta();
      meta.setDisplayName(ChatColor.DARK_GRAY + "Blanket");
      lore = Utilities.stringFixer("A blanked used to keep you warm", 35, ChatColor.GRAY);
      meta.setLore(lore);
      it.setItemMeta(meta);
      grayItems.put(CustomItemType.Blanket, it);
   }

}
