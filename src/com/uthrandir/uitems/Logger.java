/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uthrandir.uitems;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author lars
 */
public class Logger {

   public static void debug(String message) {
      UItems.inst.getServer().getConsoleSender().sendMessage(ChatColor.BLUE + "[uItems] " + message);
   }

   public static void info(String message) {
      UItems.inst.getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "[uItems] " + message);
   }

   public static void error(String message) {
      UItems.inst.getServer().getConsoleSender().sendMessage(ChatColor.DARK_RED + "[uItems] ERROR: " + message);
   }

   public static void msg(CommandSender cs, String msg) {
      cs.sendMessage(ChatColor.WHITE + "[" + ChatColor.YELLOW + ChatColor.BOLD + "u" + ChatColor.GOLD + ChatColor.BOLD + "Items" + ChatColor.RESET
         + ChatColor.WHITE + "] " + ChatColor.GOLD + msg);
   }

   public static void msg(Player p, String msg) {
      msg((CommandSender) p, msg);
   }

   public static void msgError(CommandSender cs, String msg) {
      cs.sendMessage(ChatColor.WHITE + "[" + ChatColor.YELLOW + ChatColor.BOLD + "u" + ChatColor.GOLD + ChatColor.BOLD + "Items" + ChatColor.RESET
         + ChatColor.WHITE + "] " + ChatColor.DARK_RED + msg);
   }

   public static void msgError(Player p, String msg) {
      msgError((CommandSender) p, msg);
   }

   public static void msgInfo(CommandSender cs, String msg) {
      cs.sendMessage(ChatColor.WHITE + "[" + ChatColor.YELLOW + ChatColor.BOLD + "u" + ChatColor.GOLD + ChatColor.BOLD + "Items" + ChatColor.RESET
         + ChatColor.WHITE + "] " + ChatColor.BLUE + msg);
   }

   public static void msgInfo(Player p, String msg) {
      msgInfo((CommandSender) p, msg);
   }

}
